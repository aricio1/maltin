<?php


namespace Drupal\maltin_forms\Helper;


use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Exception;

class CommonsHelper
{
  public static function calculateAge($day, $month, $year)
  {
    return (date("md", date("U", mktime(0, 0, 0, $month, $day, $year))) > date("md")
      ? ((date("Y") - $year) - 1)
      : (date("Y") - $year));
  }

  public static function newRegister($data)
  {
    $age = self::calculateAge((int)$data['day'], (int)$data['month'], (int)$data['year']);
    $birthday = strtotime($data['year'] . "-" . $data['month'] . "-" . $data['day'] . ' 00:00:00');
    if ($age < 18) {
      $tutorBirthday = strtotime($data['tutor_year'] . "-" . $data['tutor_month'] . "-" . $data['tutor_day'] . ' 00:00:00');
    }
    $password = base64_encode($data['password']);
    try {
      $user = self::getUserByEmail($data["email"]);
      if (is_array($user)) {
        return ["status" => false, "error" => "sameMail"];
      }
      $status = Database::getConnection()->insert('mt_register')->fields(
        [
          'names' => $data["name"],
          'lastnames' => $data["lastname"],
          'genre' => $data["genre"],
          'city' => $data["city"],
          'birthday' => $birthday,
          'document_type' => $data["document_type"],
          'document_number' => $data["document_number"],
          'cell_phone' => $data["phone"],
          'password' => $password,
          'email' => $data["email"],
          'tutor_names' => $data["tutor_name"] ?? NULL,
          'tutor_lastnames' => $data["tutor_lastname"] ?? NULL,
          'tutor_genre' => $data["tutor_genre"] ?? NULL,
          'tutor_city' => $data["tutor_city"] ?? NULL,
          'tutor_birthday' => $tutorBirthday ?? 0,
          'tutor_email' => $data["tutor_email"] ?? NULL,
          'habeas_data' => $data["terms"],
          'marketing' => $data["marketing"],
          'status' => $age < 18 ? 0 : 1,
          'created' => time(),
          'updated' => time(),
        ]
      )->execute();
      if ($status) {
        $newMail = \Drupal::service('plugin.manager.mail');
        $data["id"] = $status;
        $params = $data;
        if ($age < 18)
          $newMail->mail('maltin_forms', 'approval', $data['tutor_email'], 'en', $params, $reply = NULL, $send = TRUE);
        else {
          $newMail->mail('maltin_forms', 'confirmation', $data['email'], 'en', $params, $reply = NULL, $send = TRUE);
          self::sendCPD($data);
        }
      }
    } catch (Exception $e) {
      \Drupal::logger('newRegister')->error($e->getMessage());
      return ["status" => false, "error" => "sameMail"];
    }
    return ["status" => $status, "error" => false];
  }

  public static function updateRegister($data, $email)
  {
    try {
      $user = self::getUserByEmailAndStatus($email, 0);
      if (is_array($user)) {
        $status = Database::getConnection()->update('mt_register')->fields(
          [
            'habeas_data' => $data["terms"],
            'marketing' => $data["marketing"] ?? 0,
            'status' => 1,
            'updated' => time(),
          ]
        )->condition('email', $email, '=')->execute();
        self::sendCPD($user, 'children', true);
        self::sendCPD($user, 'tutor', true);
        $newMail = \Drupal::service('plugin.manager.mail');
        $newMail->mail('maltin_forms', 'confirmation', $email, 'en', $user, $reply = NULL, $send = TRUE);
      }
      return isset($status) ? $user : 'Usuario ya se encuentra aprobado o no se ha registrado';
    } catch (Exception $e) {
      \Drupal::logger('updateRegister')->error($e->getMessage());
      return 'Usuario ya se encuentra aprobado o no se ha registrado';
    }

  }

  public static function userLogin($user, $password)
  {
    try {
      $user = Database::getConnection()->select('mt_register', 'r')
        ->fields('r')
        ->condition('email', $user)
        ->condition('password', base64_encode($password))
        ->condition('status', 1)
        ->execute()->fetchAssoc();
      return is_array($user) ? true : false;
    } catch (Exception $e) {
      \Drupal::logger('userLogin')->error($e->getMessage());
      return false;
    }
  }

  public static function recoverPassword($user)
  {
    try {
      $user = self::getUserByEmailAndStatus($user, 1);
      return $user ? base64_decode($user['password']) : false;
    } catch (Exception $e) {
      \Drupal::logger('recoverPassword')->error($e->getMessage());
      return false;
    }
  }

  public static function newTikTok($email, $tiktok, $instagram)
  {
    try {
      $user = self::getUserByEmailAndStatus($email, 1);
      if (is_array($user)) {

        $status = Database::getConnection()->insert('mt_tik_tok')->fields(
          [
            'register_id' => $user['id'],
            'tik_tok' => $tiktok,
            'instagram' => $instagram ?? NULL,
            'created' => time()
          ]
        )->execute();
        return $status;

      }
    } catch (Exception $e) {
      \Drupal::logger('newTikTok')->error($e->getMessage());
      return false;
    }
    return false;
  }

  private static function __sendTD($form_data, $country, $brand, $campaign, $form, $unify, $production)
  {

    $td_env = $production ? 'prod' : 'dev';

    $http_protocol = isset($_SERVER['https']) ? 'https://' : 'http://';

    $form_data['abi_brand'] = $brand;
    $form_data['abi_campaign'] = $campaign;
    $form_data['abi_form'] = $form;
    $form_data['td_unify'] = $unify;
    $form_data['td_import_method'] = 'postback-api-1.2';
    $form_data['td_client_id'] = $_COOKIE['_td'];
    $form_data['td_url'] = $http_protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $form_data['td_host'] = $_SERVER['HTTP_HOST'];

    $td_country = $country;

    $td_apikey = $td_env !== 'prod' ? '9648/41e45454b77308046627548e0b4fe2ddbc0893d2' : '10086/9c06ed6fa48e0fb6952ed42773cca1cc1d43684e';

    $country_zone_mapping = array("nga" => "africa", "zwe" => "africa", "zaf" => "africa", "aus" => "apac", "chn" => "apac", "ind" => "apac", "jpn" => "apac", "kor" => "apac", "tha" => "apac", "vnm" => "apac", "bel" => "eur", "fra" => "eur", "deu" => "eur", "ita" => "eur", "nld" => "eur", "rus" => "eur", "esp" => "eur", "ukr" => "eur", "gbr" => "eur", "col" => "midam", "dom" => "midam", "ecu" => "midam", "slv" => "midam", "gtm" => "midam", "hnd" => "midam", "mex" => "midam", "pan" => "midam", "per" => "midam", "can" => "naz", "usa" => "naz", "arg" => "saz", "bol" => "saz", "bra" => "saz", "chl" => "saz", "ury" => "saz");

    $td_zone = $country_zone_mapping[$td_country];
    $curl = curl_init();
    //saz_source/bol_web_form
    $curl_opts = array(
      CURLOPT_URL => "https://in.treasuredata.com/postback/v3/event/{$td_zone}_source/{$td_country}_web_form",
      CURLOPT_POST => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        "X-TD-Write-Key: {$td_apikey}"
      ),
      CURLOPT_POSTFIELDS => json_encode($form_data)
    );

    curl_setopt_array($curl, $curl_opts);

    $response = @curl_exec($curl);
    $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    return $response_code;
  }

  private static function sendCPD($data, $type = '', $birthday = null)
  {
    $is_production = true;
    $purposes = array();
    if (isset($data['terms']) && $data['terms'])
      $purposes[] = 'TC-PP';
    elseif (isset($data['habeas_data']) && $data['habeas_data'])
      $purposes[] = 'TC-PP';
    if ($data['marketing']) $purposes[] = 'MARKETING-ACTIVATION';
    if ($type == 'tutor') {
      $info = array(
        "abi_firstname" => $data['tutor_names'],
        "abi_lastname" => $data['tutor_lastnames'],
        "abi_gender" => $data['tutor_genre'],
        "abi_dateofbirth" => date('Y-m-d', $data['tutor_birthday']), //
        "abi_email" => $data['tutor_email'],
        "abi_phone" => $data['cell_phone'],
        "abi_city" => $data['tutor_city'],
        "purpose_name" => $purposes,
      );
    } elseif ($type == 'children') {
      $info = array(
        "abi_firstname" => $data['names'],
        "abi_lastname" => $data['lastnames'],
        "abi_gender" => $data['genre'],
        "abi_dateofbirth" => date('Y-m-d', $data['birthday']),
        "abi_email" => $data['email'],
        "abi_phone" => $data['cell_phone'],
        "abi_city" => $data['city'],
        "purpose_name" => $purposes,
      );
    } else {
      $info = array(
        "abi_firstname" => $data['name'],
        "abi_lastname" => $data['lastname'],
        "abi_gender" => $data['genre'],
        "abi_dateofbirth" => $birthday ? date('Y-m-d', $data['brithday']) : $data['year'] . '-' . $data['month'] . '-' . $data['year'], //
        "abi_email" => $data['email'],
        "abi_phone" => $data['phone'],
        "abi_city" => $data['city'],
        "purpose_name" => $purposes,
      );
    }
    $tdstatus = self::__sendTD(
      $info,        // form data & purposes
      'bol',          // country
      'Maltin',       // brand
      "Maltin TIk Tok",           // campaign
      "Registro",   // form
      true,   // unify
      $is_production  // production flag
    );
    return $tdstatus;
  }

  private static function getUserByEmailAndStatus($email, $status)
  {
    return Database::getConnection()->select('mt_register', 'r')
      ->fields('r')
      ->condition('email', $email)
      ->condition('status', $status)->execute()->fetchAssoc();
  }

  private static function getUserByEmail($email)
  {
    return Database::getConnection()->select('mt_register', 'r')
      ->fields('r')
      ->condition('email', $email)->execute()->fetchAssoc();
  }

  public static function getAlert($type)
  {
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'alertas');
    $query->condition('field_type', $type);
    $entity_ids = $query->execute();
    $nodeId = array_values($entity_ids);
    return Node::load($nodeId[0]);
  }
}
