<?php


namespace Drupal\maltin_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Url;
use Drupal\maltin_forms\Helper\CommonsHelper;

/**
 * Implements an example form.
 */
class ApprovalForm extends FormBase
{
  /**
   * {@inheritdoc}
   */

  public function getFormId()
  {
    return 'maltin_approval_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // TODO: Implement buildForm() method.

    $form['terms'] = [
      '#type' => 'checkbox',
      '#title' => "Acepto Términos y Condiciones y Políticas de Privacidad.",
      '#prefix' => '<div class="custom-control custom-checkbox mb-4">',
      '#suffix' => "</div>",
      '#attributes' => ["class" => ["custom-control-input"]],
      '#label_attributes' => ['class' => ['custom-control-label']],
      "#required" => true,
    ];
    $form['marketing'] = [
      '#type' => 'checkbox',
      '#title' => "Deseo recibir información comercial y de eventos de Maltín.",
      '#prefix' => '<div class="custom-control custom-checkbox mb-4">',
      '#suffix' => "</div>",
      '#attributes' => ["class" => ["custom-control-input"]],
      '#label_attributes' => ['class' => ['custom-control-label']],
    ];
    $form['second_part']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Aceptar",
      '#prefix' => "<div class='btn-alert'>",
      '#suffix' => "<a href='/' class='btn btn-warning btn-rounded'>CANCELAR</a></div>",
      '#attributes' => ['class' => ['btn btn-warning btn-rounded']]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $data = $form_state->getValues();
    $email = base64_decode($_GET['email']);
    $email = explode('|',$email);
    if(isset($email[1])){
      $status = CommonsHelper::updateRegister($data,$email[1]);
      if (is_array($status)) {
        $url = Url::fromRoute('maltin_forms.mensaje64', ['email' => base64_encode($form_state->getValue('email'))]);
        $form_state->setRedirectUrl($url);
        return;
      }
      return $this->messenger()->addStatus($status);
    }
    return $this->messenger()->addStatus("Presentamos una falla en la aprobación de su cuenta por favor intente nuevamente");
  }
}
