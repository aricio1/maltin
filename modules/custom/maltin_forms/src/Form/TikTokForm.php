<?php


namespace Drupal\maltin_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Url;
use Drupal\maltin_forms\Helper\CommonsHelper;

/**
 * Implements an example form.
 */
class TikTokForm extends FormBase
{
  /**
   * {@inheritdoc}
   */

  public function getFormId()
  {
    return 'maltin_tiktok_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // TODO: Implement buildForm() method.
    $form['group'] = [
      '#type' => 'container',
      '#prefix' => '<div class="form-promo text-center">',
      '#suffix' => '</div>',
    ];
    $form['group']['tiktok'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Link TikTok"], 'class'=>['form-control mb-4']],
      '#prefix' => '<div class="md-form mb-3">',
      '#suffix' => '</div>',
      '#required' => true,
//      '#pattern' => '^(https:\/\/www.tiktok.com\/)+(.*)',
    ];
    $form['group']['instagram'] = array(
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Link Instagram"], 'class'=>['form-control mb-4']],
      '#prefix' => '<div class="md-form mb-3">',
      '#suffix' => '</div>',
//      '#pattern' => '^(https:\/\/www.instagram.com\/)+(.*)',
    );
    $form['group']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Participar",
      '#prefix' => '<div class="subtitle-promo text-center">
                      Haz click en el botón para participar
                    </div>',
      '#attributes' => ['class' => ['btn btn-warning btn-rounded']]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $email = base64_decode(urldecode($_GET['email']));
    $status = CommonsHelper::newTikTok($email, $form_state->getValue('tiktok'),$form_state->getValue('instagram'));
    if ($status) {
      $url = Url::fromRoute('maltin_forms.mensaje32');
      $form_state->setRedirectUrl($url);
      return ;
    } else {
      $this->messenger()->addStatus("Estamos presentando fallas por favir intente nuevamente despues.");
      return $this->redirect('<front>');
    }
  }
}
