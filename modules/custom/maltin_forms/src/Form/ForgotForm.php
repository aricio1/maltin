<?php


namespace Drupal\maltin_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\maltin_forms\Helper\CommonsHelper;

/**
 * Implements an example form.
 */
class ForgotForm extends FormBase
{
  /**
   * {@inheritdoc}
   */

  public function getFormId()
  {
    return 'maltin_forgot_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // TODO: Implement buildForm() method.
    $form['email'] = [
      '#type' => 'email',
      '#attributes' => ["placeholder" => ["Correo electrónico"], 'class' => ['form-control mb-4 w-80']],
      '#prefix' => '<div class="md-form mb-3">',
      '#suffix' => '</div>',
      '#required' => true,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Enviar Contraseña",
      '#attributes' => ['class' => ['btn btn-outline-primary btn-rounded waves-effect font-weight-bold']]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $password = CommonsHelper::recoverPassword($form_state->getValue('email'));
    if ($password) {
      $text = "Hemos enviado un email a tu cuenta con la contraseña";
      $this->messenger()->addStatus($text);
      $newMail = \Drupal::service('plugin.manager.mail');
      $newMail->mail('maltin_forms', 'recovery_password', $form_state->getValue('email'), 'en', ['password' => $password], $reply = NULL, $send = TRUE);
      return;
    }
    $text = "Cuenta no encontrada por favor intente nuevamente";
    $this->messenger()->addStatus($text);
    return;
  }
}
