<?php


namespace Drupal\maltin_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Url;
use Drupal\maltin_forms\Helper\CommonsHelper;

/**
 * Implements an example form.
 */
class RegisterForm extends FormBase
{
  /**
   * {@inheritdoc}
   */

  public function getFormId()
  {
    return 'maltin_register_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // TODO: Implement buildForm() method.
    $cities = [
      "Cobija" => "Cobija",
      "Cochabamba" => "Cochabamba",
      "La Paz" => "La Paz",
      "Oruro" => "Oruro",
      "Potosí" => "Potosí",
      "Santa Cruz de la Sierra" => "Santa Cruz de la Sierra",
      "Sucre" => "Sucre",
      "Tarija" => "Tarija",
      "Trinidad" => "Trinidad",
    ];
    $genre = ['Male' => 'Hombre', 'Female' => 'Mujer', 'Other' => 'Otro'];
    $day = ["01" => "1", "02" => "2", "03" => "3", "04" => "4", "05" => "5", "06" => "6", "07" => "7", "08" => "8", "09" => "9", "10" => "10", "11" => "11", "12" => "12", "13" => "13", "14" => "14", "15" => "15", "16" => "16", "17" => "17", "18" => "18", "19" => "19", "20" => "20", "21" => "21", "22" => "22", "23" => "23", "24" => "24", "25" => "25", "26" => "26", "27" => "27", "28" => "28", "29" => "29", "30" => "30", "31" => "31",];
    $month = ["1" => "Enero", "2" => "Febrero", "3" => "Marzo", "4" => "Abril", "5" => "Mayo", "6" => "Junio", "7" => "Julio", "8" => "Agosto", "9" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre",];
    $year = [];
    for ($i = 2020; $i > 1919; $i--)
      $year[$i] = $i;
    $form['name'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Nombre(s)"]],
      '#required' => true,
    ];
    $form['lastname'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Apellido(s)"]],
      '#required' => true,
    ];
    $form['genre'] = [
      '#type' => 'select',
      '#options' => $genre,
      '#attributes' => ['class' => ['select']],
      '#empty_option' => 'Generó',
      '#required' => true,
    ];
    $form['city'] = [
      '#type' => 'select',
      '#options' => $cities,
      '#attributes' => ['class' => ['select']],
      '#required' => true,
      '#empty_option' => 'Ciudad',
    ];
    $form['birthday'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row no-margin']]
    ];
    $form['birthday']['day'] = [
      '#type' => 'select',
      '#options' => $day,
      '#required' => true,
      '#empty_option' => 'Día',
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#ajax' => [
        'callback' => [$this, 'validateAgeUser'], //alternative notation
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'wrapper' => 'second-part',
      ],
    ];
    $form['birthday']['month'] = [
      '#type' => 'select',
      '#options' => $month,
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#required' => true,
      '#empty_option' => 'Mes',
      '#ajax' => [
        'callback' => [$this, 'validateAgeUser'], //alternative notation
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'wrapper' => 'second-part',
      ],
    ];
    $form['birthday']['year'] = [
      '#type' => 'select',
      '#options' => $year,
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#required' => true,
      '#empty_option' => 'Año',
      '#ajax' => [
        'callback' => [$this, 'validateAgeUser'], //alternative notation
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'wrapper' => 'second-part',
      ],
    ];
    $form['email'] = [
      '#type' => 'email',
      '#attributes' => ["placeholder" => ["Correo electrónico"]],
      '#required' => true,
    ];
    $form['password'] = array(
      '#type' => 'password',
      '#size' => 25,
      '#attributes' => ["placeholder" => ["Contraseña"]],
      '#required' => true,
    );
    $form['password-two'] = array(
      '#type' => 'password',
      '#size' => 25,
      '#attributes' => ["placeholder" => ["Repetir contraseña"]],
      '#required' => true,
    );
    $form['second_part'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['hidden'], 'id' => "second-part"],

    ];
    $form['second_part']['hr'] = array(
      '#type' => 'html_tag',
      "#tag" => "hr",
    );
    $form['second_part']['help_text'] = [
      '#type' => 'container',
      '#markup' => '<div class="text-register">
                          Estos datos son obligatorios y deben ser llenados <br>
                          <span class="text-register-fucsia"> por tus padres, tutores legales o apoderados.</span>
                        </div>',
    ];
    $form['second_part']['tutor_name'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Nombre(s)"]],

    ];
    $form['second_part']['tutor_lastname'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Apellido(s)"]],
    ];
    $form['second_part']['tutor_genre'] = [
      '#type' => 'select',
      '#options' => $genre,
      '#attributes' => ['class' => ['select']],
      '#empty_option' => 'Genero',
    ];
    $form['second_part']['tutor_city'] = [
      '#type' => 'select',
      '#options' => $cities,
      '#attributes' => ['class' => ['select']],
      '#empty_option' => 'Ciudad',

    ];
    $form['second_part']['tutor_birthday'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row no-margin']]
    ];
    $form['second_part']['tutor_birthday']['tutor_day'] = [
      '#type' => 'select',
      '#options' => $day,
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#empty_option' => 'Día',
    ];
    $form['second_part']['tutor_birthday']['tutor_month'] = [
      '#type' => 'select',
      '#options' => $month,
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#empty_option' => 'Mes',
    ];
    $form['second_part']['tutor_birthday']['tutor_year'] = [
      '#type' => 'select',
      '#options' => $year,
      '#attributes' => ['class' => ['select']],
      '#prefix' => "<div class='col no-padding'>",
      '#suffix' => "</div>",
      '#empty_option' => 'Año',
    ];
    $form['second_part']['document_type'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Carnet de identidad"], "class" => ['disabled']],
      '#value' => "Carnet de identidad",
      '#required' => true,
    ];
    $form['second_part']['document_number'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Número de Documento"]],
      '#required' => true,

    ];
    $form['second_part']['phone'] = [
      '#type' => 'textfield',
      '#attributes' => ["placeholder" => ["Número de celular"]],
//      '#pattern' => "d",
      '#required' => true,
    ];
    $form['second_part']['tutor_email'] = [
      '#type' => 'email',
      '#attributes' => ["placeholder" => ["Correo electrónico"]],
      '#empty_option' => 'Ciudad',
    ];
    $form['second_part']['title'] = [
      '#type' => 'html_tag',
      "#tag" => "label",
      '#value' => "Términos y condiciones",
      '#attributes' => ['class' => ['term-register']],
    ];
    $form['second_part']['terms'] = [
      '#type' => 'checkbox',
      '#title' => "Acepto Términos y Condiciones y Políticas de Privacidad.",
      '#prefix' => '<div class="custom-control custom-checkbox">',
      '#suffix' => "</div>",
      '#attributes' => ["class" => ["custom-control-input"]],
      '#label_attributes' => ['class' => ['custom-control-label check-register']],
      "#required" => true,
    ];
    $form['second_part']['marketing'] = [
      '#type' => 'checkbox',
      '#title' => "Deseo recibir información comercial y de eventos de Maltín.",
      '#prefix' => '<div class="custom-control custom-checkbox">',
      '#suffix' => "</div>",
      '#attributes' => ["class" => ["custom-control-input"]],
      '#label_attributes' => ['class' => ['custom-control-label check-register']],
    ];
    $form['second_part']['captcha'] = [
      '#type' => 'captcha',
      '#title' => 'Contesta la siguiente pregunta',
      '#captcha_type' => 'default',
    ];
    $form['second_part']['help'] = [
      '#type' => 'container',
      '#markup' => '<div class="text-register text-register-fucsia" style="margin-top: 20px;">
                          Te va a llegar un correo que le parece a tu padres para terminar y aceptar el registro del concurso.
                        </div>',
    ];
    $form['second_part']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "REGISTRARME",
      '#prefix' => "<p class='text-center' style='margin-top: 20px;'>",
      '#suffix' => "</p>",
      '#attributes' => ['class' => ['btn btn-secondary']]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $data = $form_state->getValues();
    $age = CommonsHelper::calculateAge((int) $data['tutor_day'], (int) $data['tutor_month'], (int) $data['tutor_year']);
    if($data['password'] != $data['password-two']){
      $form['birthday']['day']['#value']='';
      $form['birthday']['month']['#value']='';
      $form['birthday']['year']['#value']='';
      $form_state->unsetValue('day');
      $form_state->unsetValue('month');
      $form_state->unsetValue('year');
      $form_state->setErrorByName('password-two', 'Las contraseñas no coinciden intente nuevamente por favor.');
    }
    if($age < 18)
    {
      $form['birthday']['day']['#value']='';
      $form['birthday']['month']['#value']='';
      $form['birthday']['year']['#value']='';
      $form_state->unsetValue('day');
      $form_state->unsetValue('month');
      $form_state->unsetValue('year');
      $form_state->setErrorByName('tutor_year', "El tutor debe ser mayor de edad.");
    }
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $data = $form_state->getValues();
    $status = CommonsHelper::newRegister($data);
    $age = CommonsHelper::calculateAge((int) $data['day'], (int) $data['month'], (int) $data['year']);
    $nameUrl = $age < 18 ? 'maltin_forms.mensaje61' : 'maltin_forms.mensaje6';
    if ($status['status']) {
      $url = Url::fromRoute($nameUrl, ['email' => base64_encode($form_state->getValue('email'))]);
      $form_state->setRedirectUrl($url);
      return;
    }
    elseif ($status['error'] == 'sameMail'){
      return $this->messenger()->addStatus("El correo electrónico que usaste ya existe. Por favor, intenta con otro");
    }
    return $this->messenger()->addStatus("Presentamos una falla en la creación de su cuenta por favor intente nuevamente");

//
  }

  public function validateAgeUser(array &$form, FormStateInterface $form_state)
  {
    $day = $form_state->getValue('day');
    $month = $form_state->getValue('month');
    $year = $form_state->getValue('year');
    if ($day && $month && $year) {
      $age = CommonsHelper::calculateAge($day, $month, $year);
      if ($age >= 18) {
        $form['second_part']['tutor_name'] = ['#attributes' => ['class' => ['hidden']],];
        $form['second_part']['tutor_lastname'] = ['#attributes' => ['class' => ['hidden']],];
        $form['second_part']['tutor_genre'] = ['#attributes' => ['class' => ['hidden']],];
        $form['second_part']['tutor_birthday'] = ['#attributes' => ['class' => ['hidden']]];
        $form['second_part']['help_text'] = ['#attributes' => ['class' => ['hidden']]];
        $form['second_part']['help'] = ['#attributes' => ['class' => ['hidden']]];
        $form['second_part']['tutor_email'] = ['#attributes' => ['class' => ['hidden']],];
        $form['second_part']['tutor_city'] = ['#attributes' => ['class' => ['hidden']],];
      } else {
        $form['second_part']['tutor_name']["#required"] = true;
        $form['second_part']['tutor_lastname']["#required"] = true;
        $form['second_part']['tutor_genre']["#required"] = true;
        $form['second_part']['tutor_birthday']['tutor_day']["#required"] = true;
        $form['second_part']['tutor_birthday']['tutor_month']["#required"] = true;
        $form['second_part']['tutor_birthday']['tutor_year']["#required"] = true;
//        $form['second_part']['tutor_birthday'] = ['#attributes' => ['class' => ['hidden']]];
        $form['second_part']['tutor_email']["#required"] = true;
        $form['second_part']['tutor_city']["#required"] = true;
      }
      $form['second_part']['#attributes']['class'] = [''];
    }
    return $form['second_part'];
  }

}
