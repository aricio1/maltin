<?php


namespace Drupal\maltin_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\maltin_forms\Helper\CommonsHelper;

/**
 * Implements an example form.
 */
class LoginForm extends FormBase
{
  /**
   * {@inheritdoc}
   */

  public function getFormId()
  {
    return 'maltin_login_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // TODO: Implement buildForm() method.
    $form['email'] = [
      '#type' => 'email',
      '#attributes' => ["placeholder" => ["Correo electrónico"], 'class' => ['form-control mb-4 w-80']],
      '#prefix' => '<div class="md-form mb-3">',
      '#suffix' => '</div>',
      '#required' => true,
    ];
    $form['password'] = array(
      '#type' => 'password',
      '#size' => 25,
      '#attributes' => ["placeholder" => ["Contraseña"], 'class' => ['form-control mb-4 w-80']],
      '#prefix' => '<div class="md-form mb-3">',
      '#suffix' => '</div>',
      '#required' => true,
    );
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Iniciar sesión",
      '#prefix' => '<a href="/recuperar-contrasena"><h6 class="modal-title w-100 font-weight-bold text-center link-login">Olvidé mi contraseña</h6></a>',
      '#attributes' => ['class' => ['btn btn-outline-primary btn-rounded waves-effect font-weight-bold']]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $status = CommonsHelper::userLogin($form_state->getValue('email'), $form_state->getValue('password'));
    if ($status) {
      $url = Url::fromRoute('maltin_forms.tiktok', ['email' => base64_encode($form_state->getValue('email'))]);
      $form_state->setRedirectUrl($url);
      return ;
    } else {
      $this->messenger()->addStatus("Email o contraseña no coinciden");
      return $this->redirect('<front>');
    }
  }
}
