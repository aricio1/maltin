<?php

namespace Drupal\maltin_forms\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\maltin_forms\Helper\CommonsHelper;

class MaltinController extends ControllerBase
{
  public function getAlert32()
  {
    $node = CommonsHelper::getAlert('32');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "ir a tiktok",
      '#type' => 32,
    ];
  }

  public function getAlert33()
  {
    $node = CommonsHelper::getAlert('33');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "ir",
      '#type' => 33,
    ];
  }

  public function getAlert61()
  {
    $node = CommonsHelper::getAlert('61');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "Cerrar",
      '#type' => 61,
    ];
  }

  public function getAlert62()
  {
    $node = CommonsHelper::getAlert('62');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "Cerrar",
      '#type' => 62,
    ];
  }

  public function getAlert64()
  {
    $node = CommonsHelper::getAlert('64');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "Cerrar",
      '#type' => 64,
    ];
  }

  public function getAlert6()
  {
    $node = CommonsHelper::getAlert('6');
    return [
      '#theme' => 'messages',
      '#subtitle' => $node->field_subtitle->value,
      '#message1' => $node->field_message1->value,
      '#message2' => $node->field_message2->value,
      '#url' => $node->field_url,
      '#namelink' => "Cerrar",
      '#type' => 6,
    ];
  }

}
