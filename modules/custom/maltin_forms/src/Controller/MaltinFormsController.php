<?php


namespace Drupal\maltin_forms\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MaltinFormsController extends ControllerBase
{
  public function getRegistrationForm()
  {
    $myform = \Drupal::formBuilder()->getForm('Drupal\maltin_forms\Form\RegisterForm');
    // If you want modify the form:
    $myform['field']['#value'] = 'From my controller';

    return [
      '#theme' => 'register_form',
      '#form' => $myform,
    ];
  }
  public function getLoginForm()
  {
    $myform = \Drupal::formBuilder()->getForm('Drupal\maltin_forms\Form\LoginForm');

    return [
      '#theme' => 'login_form',
      '#form' => $myform,
    ];
  }
  public function getForgotForm()
  {
    $myform = \Drupal::formBuilder()->getForm('Drupal\maltin_forms\Form\ForgotForm');

    return [
      '#theme' => 'forgot_form',
      '#form' => $myform,
    ];
  }
  public function getApprovalForm()
  {
    if(isset($_GET['email'])) {
      $myform = \Drupal::formBuilder()->getForm('Drupal\maltin_forms\Form\ApprovalForm');

      return [
        '#theme' => 'approval_form',
        '#form' => $myform,
      ];
    }
    return $this->redirect('<front>');
  }
  public function getTikTokForm()
  {
    if(isset($_GET['email']))
    {
      $myform = \Drupal::formBuilder()->getForm('Drupal\maltin_forms\Form\TikTokForm');
      return [
        '#theme' => 'tiktok_form',
        '#form' => $myform,
      ];
    }
    return $this->redirect('<front>');
  }

}
