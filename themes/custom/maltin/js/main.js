(function ($) {

  $("#modalRegisterForm").on('hidden.bs.modal', function () {
    $('#modalRegisterForm2').modal('show')
  });

  $(document).ready(function () {

// SideNav Button Initialization
    $(".button-collapse").sideNav({
      closeOnClick: true, // Closes side-nav on &lt;a&gt; clicks, useful for Angular/Meteor
      edge: 'right', // Choose the horizontal origin
      breakpoint: 3000
    });

// SideNav Scrollbar Initialization
    var sideNavScrollbar = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(sideNavScrollbar);

// Evento Traqueo

var element = document.getElementById("maltin-register-form");
if (element)
  element.onsubmit = function() {myFunctiontraker()};

function myFunctiontraker() {

  dataLayer.push({
    'event': 'trackEvent',
    'eventCategory': 'Registro', // Categoría del evento (String). Requerido.
    'eventAction': 'clic', // Acción o subcategoría del evento (String). Requerido.
    'eventLabel': 'Registrarme', // Etiqueta de descripción del evento (String). Requerido.
    'eventValue': '_VALOR_' // Valor o peso (importancia) del evento (String).
    });
}


  })
})(jQuery);
